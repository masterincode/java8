package java8.problems;

import java.util.function.Predicate;

public class Lecture31DisplayNameStartsWithLetterKUsingPredicate {


    public static void main(String[] args) {
        displayName();
    }

    public static void displayName() {
        String[] names = {"Sunny","Kajal","Mallika","Katrina","Kareena"};

        for(String name: names) {
            Predicate<String> predicate = singleName -> singleName.charAt(0) == 'K';
            if(predicate.test(name))
                System.out.println("Name: "+name);
        }
    }

}

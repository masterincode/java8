package java8.problems;

import java.util.function.Predicate;

class User{

    String username;
    String password;
    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
public class Lecture33UserAuthenticationUsingUserObject {

    public static void main(String[] args) {
        authenticate();
    }

    public static void authenticate() {
        User user = new User("Balmukund", "Asdf1234");

        Predicate<User> predicate = singleUser -> singleUser.username.equalsIgnoreCase("Balmukund") &&
                singleUser.password.equalsIgnoreCase("Asdf1234");
        if(predicate.test(user))
            System.out.println("Authentic User!!");
        else
            System.out.println("Username or Password is wrong!!");

    }
}

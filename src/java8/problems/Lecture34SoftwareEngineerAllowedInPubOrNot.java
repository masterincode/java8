package java8.problems;

import java.util.function.Predicate;

class Engineer {
    String name;
    int age;
    boolean isHavingGf;
    public Engineer(String name, int age, boolean isHavingGf) {
        this.name = name;
        this.age = age;
        this.isHavingGf = isHavingGf;
    }
}
public class Lecture34SoftwareEngineerAllowedInPubOrNot {

    public static void main(String[] args) {
       allowedOrNot();
    }

    public static void allowedOrNot() {
        Engineer[] engineers = { new Engineer("Durga", 60, false),
                                 new Engineer("Sunil", 25 , true),
                                 new Engineer("Sayan", 26, true),
                                 new Engineer("Subbu", 28, false),
                                 new Engineer("Ravi", 19, true)
                                 };

        Predicate<Engineer> predicate = engineer -> engineer.age > 18 && engineer.isHavingGf == true;
        for(Engineer eng: engineers) {
            if(predicate.test(eng))
                System.out.println("Allowed to Pub: "+eng.name);
        }

    }
}

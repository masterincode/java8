package java8.problems;

import java.util.function.BiFunction;

public class Lecture62CalculateSalaryUsingEmployeeAndTimeSheet {

    public static void main(String[] args) {
        Employee employee = new Employee(101,"Sachin", 1000);
        TimeSheet timeSheet = new TimeSheet(101, 30);
        BiFunction<Employee,TimeSheet,Double> biFunction = (e, ts) -> e.dailyWage * ts.days;
        System.out.println("Monthly Salary: "+biFunction.apply(employee,timeSheet));
    }
}
class Employee{
    public int empNo;
    public String empName;
    public double dailyWage;

    public Employee(int empNo, String empName, double dailyWage) {
        this.empNo = empNo;
        this.empName = empName;
        this.dailyWage = dailyWage;
    }
}

class TimeSheet {
    public int empNo;
    public int days;

    public TimeSheet(int empNo, int days) {
        this.empNo = empNo;
        this.days = days;
    }
}

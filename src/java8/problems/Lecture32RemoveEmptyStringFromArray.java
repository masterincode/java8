package java8.problems;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class Lecture32RemoveEmptyStringFromArray {

    public static void main(String[] args) {
        removeEmptyString();
    }

    public static void removeEmptyString() {

        String[] names ={"Durga", "", null, "Ravi", "", "Shiva", null};
        List<String> list =  new ArrayList<>();
        Predicate<String> predicate = singleName -> singleName == null || singleName.equalsIgnoreCase("") ;
        for(String name: names) {
            if(!predicate.test(name)) {
                list.add(name);
            }
        }
        for(String name: list)
            System.out.println("Name: "+name);
    }
}

package java8.problems;

import java.util.function.BiConsumer;

public class Lecture63BiConsumer {

    public static void main(String[] args) {

        BiConsumer<String, String> biConsumer = (arg1, arg2) -> System.out.println(arg1.concat(arg2)) ;
        biConsumer.accept("Balmukund ", "Singh");
    }
}
